INSERT INTO spendingcalculator.spendingcategory (id, name) VALUES ('-1', 'élelmiszer');
INSERT INTO spendingcalculator.spendingcategory (id, name) VALUES ('-2', 'lakhatás');
Commit;
INSERT INTO spendingcalculator.spending (id, date, description, price, spendingcategory_id) VALUES ('-1', '2019.07.01', 'lakbér', '70000', '-2');
INSERT INTO spendingcalculator.spending (id, date, description, price, spendingcategory_id) VALUES ('-2', '2019.07.04', 'pékség', '300', '-1');
INSERT INTO spendingcalculator.spending (id, date, description, price, spendingcategory_id) VALUES ('-3', '2019.07.04', 'ebéd', '1200', '-1');
INSERT INTO spendingcalculator.spending (id, date, description, price, spendingcategory_id) VALUES ('-4', '2019.07.05', 'vacsora', '2500', '-1');
INSERT INTO spendingcalculator.spending (id, date, description, price, spendingcategory_id) VALUES ('-5', '2019.07.09', 'rezsi', '30000', '-2');
Commit;
