package hu.horvathbzs.spendingcalculator.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpendingCategoryDto {
    
    private Long id;
    private String name;
    private List<SpendingDto> spendingDtos = new ArrayList<>();
}
