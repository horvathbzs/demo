package hu.horvathbzs.spendingcalculator.dto.mapper;

import hu.horvathbzs.spendingcalculator.dto.SpendingDto;
import hu.horvathbzs.spendingcalculator.entity.Spending;

/**
 *
 * @author horvathbzs
 */


public class SpendingMapper implements EntityDtoMapper<Spending, SpendingDto>{

    @Override
    public Spending mapDtoToEntity(SpendingDto dto) {
        
        Spending entity = new Spending();
        
        entity.setId(dto.getId());
        entity.setDate(dto.getDate());
        entity.setDescription(dto.getDescription());
        entity.setPrice(dto.getPrice());
        entity.setSpendingCategory(dto.getSpendingCategoryDto());
        
        return entity;
    }

    @Override
    public SpendingDto mapEntityToDto(Spending entity) {
        
        SpendingDto dto = new SpendingDto();
        
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setDescription(entity.getDescription());
        dto.setPrice(entity.getPrice());
        dto.setSpendingCategoryDto(entity.getSpendingCategory());
        return dto;
    } 
}
