package hu.horvathbzs.spendingcalculator.dto;

import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpendingDto {
    
    private Long id;
    private Date date;
    private String description;
    private Integer price;
    private SpendingCategory spendingCategoryDto;
    
}
