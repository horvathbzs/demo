package hu.horvathbzs.spendingcalculator.dto.mapper;

import hu.horvathbzs.spendingcalculator.dto.SpendingCategoryDto;
import hu.horvathbzs.spendingcalculator.dto.SpendingDto;
import hu.horvathbzs.spendingcalculator.entity.Spending;
import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author horvathbzs
 */
public class SpendingCategoryMapper
        implements EntityDtoMapper<SpendingCategory, SpendingCategoryDto>{

    SpendingMapper spendingMapper = new SpendingMapper();
    
    @Override
    public SpendingCategory mapDtoToEntity(SpendingCategoryDto dto) {
        
        SpendingCategory entity = new SpendingCategory();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        
        List<Spending> spendingEntities = new ArrayList<>();
        
        for (int i = 0; i < entity.getSpendings().size() - 1; i++) {
            spendingEntities.add(spendingMapper.mapDtoToEntity(dto.getSpendingDtos().get(i)));
            
        }
        
        return entity;
    }

    @Override
    public SpendingCategoryDto mapEntityToDto(SpendingCategory entity) {
        
        SpendingCategoryDto dto = new SpendingCategoryDto();
        
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        
        List<SpendingDto> spendingDtos = new ArrayList<>();
        
        for (int i = 0; i < entity.getSpendings().size() - 1; i++) {
            spendingDtos.add(spendingMapper.mapEntityToDto(entity.getSpendings().get(i)));
            
        }
        
        return dto;
    }
}