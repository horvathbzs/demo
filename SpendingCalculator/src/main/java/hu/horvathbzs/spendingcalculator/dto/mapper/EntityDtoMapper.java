package hu.horvathbzs.spendingcalculator.dto.mapper;

/**
 *
 * @author horvathbzs
 */
public interface EntityDtoMapper<Entity, Dto> {
    
    Entity mapDtoToEntity(Dto dto);
    
    Dto mapEntityToDto(Entity entity);
}
