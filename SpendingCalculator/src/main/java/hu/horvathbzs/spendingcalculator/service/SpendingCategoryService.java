package hu.horvathbzs.spendingcalculator.service;

import hu.horvathbzs.spendingcalculator.dao.SpendingCategoryDao;
import hu.horvathbzs.spendingcalculator.dto.SpendingCategoryDto;
import hu.horvathbzs.spendingcalculator.dto.mapper.SpendingCategoryMapper;
import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author horvathbzs
 */

@Singleton
public class SpendingCategoryService {
 
    @EJB
    private SpendingCategoryDao spendingCategoryDao;
    
    private SpendingCategoryMapper spendingCategoryMapper = new SpendingCategoryMapper();
    
    public void save(SpendingCategoryDto spendingCategoryDto) {
        spendingCategoryDao.save(spendingCategoryMapper.mapDtoToEntity(spendingCategoryDto));
    }
    
    public List<SpendingCategoryDto> getAll() {
        
        List<SpendingCategory> entities = spendingCategoryDao.getAll();
        List<SpendingCategoryDto> dtos = new ArrayList<>();
        
        for (int i = 0; i < entities.size(); i++) {
            dtos.add(spendingCategoryMapper.mapEntityToDto(entities.get(i)));
        }
        
        return dtos;
    }
}
