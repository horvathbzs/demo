package hu.horvathbzs.spendingcalculator.service;

import hu.horvathbzs.spendingcalculator.dao.SpendingCategoryDao;
import hu.horvathbzs.spendingcalculator.dao.SpendingDao;
import hu.horvathbzs.spendingcalculator.dto.SpendingDto;
import hu.horvathbzs.spendingcalculator.dto.mapper.SpendingMapper;
import hu.horvathbzs.spendingcalculator.entity.Spending;
import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author horvathbzs
 */

@Singleton
public class SpendingService {
    
    
    @EJB
    private SpendingDao spendingDao;
    
    @EJB
    private SpendingCategoryDao spendingCategoryDao;
    
    private SpendingMapper spendingMapper = new SpendingMapper();

    public void save(SpendingDto spendingDto) {
        spendingDao.save(spendingMapper.mapDtoToEntity(spendingDto));
    }
    
    public List<SpendingDto> getAll() {
        
        List<Spending> entities = spendingDao.getAll();
        List<SpendingDto> dtos = new ArrayList<>();
        
        for (int i = 0; i < entities.size() - 1; i++) {
            dtos.add(spendingMapper.mapEntityToDto(entities.get(i)));
        }
        
        return dtos;
    }
}
