package hu.horvathbzs.spendingcalculator.dao;

import hu.horvathbzs.spendingcalculator.entity.Spending;
import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 */
@Stateless
@LocalBean
public class SpendingDao implements Dao<Spending> {

    @PersistenceContext(unitName = "SpendingCalculatorPU")
    EntityManager em;

    @Override
    public List<Spending> getAll() {
        Query q = em.createQuery("Select s from Spending s", Spending.class);
        return q.getResultList();
    }

    @Override
    public void save(Spending spending) {
        em.persist(spending);
    }

    @Override
    public void delete(Spending spending) {
        em.remove(spending);
    }

    @Override
    public Optional<Spending> get(Long id) {
        return Optional.ofNullable(em.find(Spending.class, id));
    }

    @Override
    public void update(Spending spending) {
        em.merge(spending);
    }
    
    @Override    
    public List<Spending> getEntityByColumn(Class<Spending> entityClass, String colName, String value) {
        Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value));
        return q.setParameter(PARAM_VALUE, value).getResultList();
    }
}
