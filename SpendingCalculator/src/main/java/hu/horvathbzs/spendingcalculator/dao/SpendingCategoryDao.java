package hu.horvathbzs.spendingcalculator.dao;

import static hu.horvathbzs.spendingcalculator.dao.Dao.JPQL_GET_ENTITY_BY_COLNAME;
import static hu.horvathbzs.spendingcalculator.dao.Dao.PARAM_VALUE;
import hu.horvathbzs.spendingcalculator.entity.SpendingCategory;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 */

@Stateless
@LocalBean
public class SpendingCategoryDao implements Dao<SpendingCategory>{
    
    @PersistenceContext(unitName = "SpendingCalculatorPU")
    EntityManager em;

    @Override
    public List<SpendingCategory> getAll() {
        Query q = em.createQuery("Select sc from SpendingCategory sc", SpendingCategory.class);
        return q.getResultList();
    }

    @Override
    public void save(SpendingCategory spendingCategory) {
        em.persist(spendingCategory);
    }

    @Override
    public void delete(SpendingCategory spendingCategory) {
        em.remove(spendingCategory);
    }

    @Override
    public Optional<SpendingCategory> get(Long id) {
        return Optional.ofNullable(em.find(SpendingCategory.class, id));
    }

    @Override
    public void update(SpendingCategory spendingCategory) {
        em.merge(spendingCategory);
    }
    
    @Override    
    public List<SpendingCategory> getEntityByColumn(Class<SpendingCategory> entityClass, String colName, String value) {
        Query q = em.createQuery(String.format(JPQL_GET_ENTITY_BY_COLNAME, entityClass.getSimpleName(), colName, value));
        return q.setParameter(PARAM_VALUE, value).getResultList();
    }
}
