package hu.horvathbzs.spendingcalculator.dao;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 * @param <T>
 */
public interface Dao<T> {

    String PARAM_VALUE = "param";
    String JPQL_GET_ENTITY_BY_COLNAME = "Select e from %s e where e.%s = :" + PARAM_VALUE;

    List<T> getAll();

    void save(T t);

    void delete(T t);

    Optional<T> get(Long id);

    void update(T t);
    
    List<T> getEntityByColumn(Class<T> entityClass, String colName, String value);

}
