package hu.horvathbzs.spendingcalculator.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */
@Entity
@Table(name = "spendingcategory")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpendingCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String name;
    
    @OneToMany(mappedBy = "spendingCategory")
    private List<Spending> spendings = new ArrayList<>();

    public SpendingCategory(Long id) {
        this.id = id;
    }
}
