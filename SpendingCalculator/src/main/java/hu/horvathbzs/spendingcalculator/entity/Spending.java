package hu.horvathbzs.spendingcalculator.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */
@Entity
@Table(name = "spending")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Spending implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date date;

    private String description;
    
    private Integer price;
    
    @ManyToOne
    @JoinColumn(name="spendingcategory_id")
    private SpendingCategory spendingCategory;
}
