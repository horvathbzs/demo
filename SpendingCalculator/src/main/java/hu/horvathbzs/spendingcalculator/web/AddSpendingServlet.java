package hu.horvathbzs.spendingcalculator.web;

import hu.horvathbzs.spendingcalculator.dto.SpendingCategoryDto;
import hu.horvathbzs.spendingcalculator.dto.SpendingDto;
import hu.horvathbzs.spendingcalculator.service.SpendingCategoryService;
import hu.horvathbzs.spendingcalculator.service.SpendingService;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author horvathbzs
 */
@WebServlet(name = "AddSpendingServlet", urlPatterns = {"/add_spending"})
public class AddSpendingServlet extends HttpServlet {

    @EJB
    private SpendingService spendingService;

    @EJB
    private SpendingCategoryService spendingCategoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<SpendingCategoryDto> spendingCategories = spendingCategoryService.getAll();
        request.setAttribute("formCategory", spendingCategories);
        System.out.println(spendingCategories);
        request.getRequestDispatcher("./WEB-INF/page/add_spending.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
