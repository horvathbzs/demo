<%-- 
    Document   : spending
    Created on : 2019.07.26., 23:17:05
    Author     : horvathbzs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

    <head>

        <%@include file="../asset/include/headBase.jsp" %>

        <title>Költés</title>
    </head>

    <body>
        <div class="container">
            <h1>Kiadás</h1>

            <form method="POST">
                <div class="form-group">
                    <label for="formDate">Dátum</label>
                    <input type="date" class="form-control" id="formDate" name="formDate" placeholder="">
                </div>
                <div class="form-group">
                    <label for="formCategory">Kategória</label>
                    <select class="form-control" id="formCategory" name="formCategory">
                        <c:forEach items="${spendingCategories}" var="i">
                            <option value="${i.id}">${i.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label for="formDescription">Megnevezés</label>
                    <input type="text" class="form-control" id="formDescription" name="formDescription"
                           placeholder="Megnevezés">
                </div>
                <div class="form-group">
                    <label for="formQuantity">Mennyiség (Ft)</label>
                    <input type="text" class="form-control" id="formQuantity" name="formQuantity" placeholder="Forint">
                </div>
                <button class="btn btn-primary" type="submit">Mentés</button>
            </form>
        </div>

    </body>

</html>