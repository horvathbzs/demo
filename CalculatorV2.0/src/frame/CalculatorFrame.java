

package frame;

import calculator.OperationSigns;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.TextField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import operation.OperationHandler;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class CalculatorFrame extends JFrame {
    
    private OperationHandler operationHandler;
    
    private JTextField tfCalculationLine;
    private JTextField tfDisplayNumber;
    private JTextArea taCalculationHistory;
    

    public CalculatorFrame(OperationHandler operationHandler) throws HeadlessException {
        
        this.operationHandler = operationHandler;
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 400);
        setLocationRelativeTo(this);
        
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        
        JScrollPane jsp = new JScrollPane();
        
        tfCalculationLine = new JTextField();
        tfCalculationLine.setHorizontalAlignment(JTextField.RIGHT);
        tfDisplayNumber = new JTextField("0");
        tfDisplayNumber.setHorizontalAlignment(JTextField.RIGHT);
        taCalculationHistory = new JTextArea();
        taCalculationHistory.add(jsp);
        tfCalculationLine.setEditable(false);
        tfDisplayNumber.setEditable(false);
        taCalculationHistory.setEditable(false);
        
        setUpTfCalculationLine();
        setUpTfResult();
        setUpNumberButtons();
        setUpZeroButton();
        setUpClearButton();
        setUpHistoryButton();
        setUpSummationButton();
        setUpSubtractionButton();
        setUpMultiplicationButton();
        setUpDivisionButton();
        setUpDecimalSeparatorButton();
        setUpResultButton();
        setUpTaCalculationHistory();
         
        setVisible(true);
    }

    public JTextField getTfCalculationLine() {
        return tfCalculationLine;
    }

    public JTextField getTfDisplayNumber() {
        return tfDisplayNumber;
    }

    public JTextArea getTaCalculationHistory() {
        return taCalculationHistory;
    }

    private void setUpTfCalculationLine() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        this.add(tfCalculationLine, gbc);
    }
    
    private void setUpTfResult() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        this.add(tfDisplayNumber, gbc); 
    }
    
    private void setUpNumberButtons() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;
        for (int i = 0, k = 7; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                JButton btNumbers = new JButton();
                btNumbers.setText("" + k++);
                gbc.gridx = j;
                gbc.gridy = i + 2;
                btNumbers.addActionListener(
                        number -> operationHandler.handleNumberButtonClick(btNumbers.getText())
                );
                this.add(btNumbers, gbc);
            }
            k -= 6;
        }
    }
    
    private void setUpZeroButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btZero = new JButton();
        btZero.setText("0");
        btZero.addActionListener(zero -> operationHandler.handleNumberButtonClick(btZero.getText()));
        this.add(btZero, gbc);
        
    }
    
    private void setUpClearButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btClear = new JButton();
        btClear.setText(OperationSigns.CLEAR.getSign());
        btClear.addActionListener(clear -> operationHandler.handleClearButtonClick());
        this.add(btClear, gbc);
    }
    
    private void setUpHistoryButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridheight = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btHistory = new JButton();
        btHistory.setText("Előzmények");
        btHistory.addActionListener(history -> operationHandler.handleHistoryButtonClick());
        this.add(btHistory, gbc);
    }
    
    private void setUpSummationButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btSummation = new JButton();
        btSummation.setText(OperationSigns.SUMMATION.getSign());
        btSummation.addActionListener(sum -> operationHandler.handleSummationButtonClick());
        this.add(btSummation, gbc);
    }
    
    private void setUpSubtractionButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 3;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btSubtraction = new JButton();
        btSubtraction.setText(OperationSigns.SUBTRACTION.getSign());
        btSubtraction.addActionListener(subtract -> operationHandler.handleSubtractionButtonClick());
        this.add(btSubtraction, gbc);
    }
    
    private void setUpMultiplicationButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btMultiplication = new JButton();
        btMultiplication.setText(OperationSigns.MULTIPLCATION.getSign());
        btMultiplication.addActionListener(multiply -> operationHandler.handleMultiplicationButtonClick());
        this.add(btMultiplication, gbc);
    }
    
    private void setUpDivisionButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btDivision = new JButton();
        btDivision.setText(OperationSigns.DIVISION.getSign());
        btDivision.addActionListener(devide -> operationHandler.handleDivisionButtonClick());
        this.add(btDivision, gbc);
    }
    
    private void setUpDecimalSeparatorButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btDecimalSeparator = new JButton();
        btDecimalSeparator.setText(OperationSigns.DECIMALSEPARATOR.getSign());
        btDecimalSeparator.addActionListener(
                decimalSeparator -> operationHandler.handleNumberButtonClick(btDecimalSeparator.getText())
        );
        this.add(btDecimalSeparator, gbc);
    }
    
    private void setUpResultButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        JButton btResult = new JButton();
        btResult.setText(OperationSigns.RESULT.getSign());
        btResult.addActionListener(result -> operationHandler.handleResultButtonClick());
        this.add(btResult, gbc);
    }
    
    private void setUpTaCalculationHistory() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridheight = 1;
        gbc.gridwidth = 5;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        this.add(taCalculationHistory, gbc);
    }
    
}
