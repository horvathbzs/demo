

package logger;

import java.util.List;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class Logger {
    
    private File logFile = new File("." + File.separator + "log" + File.separator + "log.txt");
    
    
    private List<String> linesFromLogFile = new ArrayList<>();

    //mindekeppen kell privat konstruktor!!!
    private Logger() {
    }

    private static class LogHolder {
        private static final Logger INSTANCE = new Logger();
    }
    
    public static Logger getInstance () {
        return LogHolder.INSTANCE;
    }

    public List<String> getLinesFromLogFile() {
        return linesFromLogFile;
    }
    
    public void writeLog(String logText) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(logFile, true))) {    
            pw.println(logText);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void readLog() {   
        linesFromLogFile.clear();
        try (BufferedReader br = new BufferedReader(new FileReader(logFile))) { 
            String lineInFile;
            while ((lineInFile = br.readLine()) != null) {
                linesFromLogFile.add(lineInFile);
            }     
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException ex) {
            System.out.println(ex);
        } 
    }
    
      
//    public void readLogInputStream() {       
//        try (FileInputStream fis = new FileInputStream(logFile);
//                DataInputStream dis = new DataInputStream(fis)) {
//            System.out.println(logFile.length());
////            for (int i = 0; i < logFile.length() / 4; i++) {
////                System.out.println(dis.readChar());
////            }
//        } catch (FileNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
