

package operation;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class MathematicOperatoinsUtil {

    public static Double sum(Double a, Double b) {
        return a + b;
    }
    
    public static Double subtract(Double a, Double b) {
        return a - b;
    }
    
    public static Double multiply(Double a, Double b) {
        return a * b;
    }
    
    public static Double devide(Double a, Double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("Nullával nem lehet osztani.");
        }
        return a / b;
    }
}
