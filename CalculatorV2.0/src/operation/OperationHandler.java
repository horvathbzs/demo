

package operation;

import calculator.Calculator;
import calculator.OperationSigns;
import frame.CalculatorFrame;
import java.util.stream.Collectors;
import logger.Logger;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class OperationHandler {

    private Calculator calculator;
    private CalculatorFrame calculatorFrame;
    private Logger logger;
    
    private StringBuffer displayNumberAsTextCache = new StringBuffer("");
    private StringBuffer logTextCache = new StringBuffer("");

    public OperationHandler() {
        this.calculator = new Calculator();
        this.calculatorFrame = new CalculatorFrame(this);
        this.logger = Logger.getInstance();
    }
    
    private void formattedAppendLogTextCache(OperationSigns operationSign) throws NumberFormatException {
        logTextCache.append(" ")
                    .append(operationSign.getSign())
                    .append(" "); 
    }
    
    private void resetDisplayTextCache() {
        displayNumberAsTextCache.delete(0, displayNumberAsTextCache.length());
    }
    
    private void resetLogTextCache() {
        logTextCache.delete(0, logTextCache.length());
    }
    
    private void resetAll() {
        resetDisplayTextCache();
        resetLogTextCache();
        calculator.resetCalculator();
    }
    
    private void checkIfDecimalSeparatorPoint() {
        int indexOfDecimalSeparator = displayNumberAsTextCache.indexOf(OperationSigns.DECIMALSEPARATOR.getSign());
        if ( ! OperationSigns.DECIMALSEPARATOR.getSign().equals(".") &&
                indexOfDecimalSeparator != -1) {
            displayNumberAsTextCache.replace(indexOfDecimalSeparator, indexOfDecimalSeparator + 1, ".");
        } 
    }
    
    private void operation(Double resultMathematicOperation) {
        calculator.setNumberInMemory(
                resultMathematicOperation
        );
        calculator.setDisplayNumber(calculator.getNumberInMemory());
    }
    
    private void updateViewFromModel() {
        calculatorFrame.getTfDisplayNumber().setText(calculator.getDisplayNumber().toString());
        calculatorFrame.getTfCalculationLine().setText(logTextCache.toString());
    }
    
    public void handleNumberButtonClick(String numberButton) {
        
        displayNumberAsTextCache.append(numberButton);
        logTextCache.append(numberButton);
        
        checkIfDecimalSeparatorPoint();
        
        calculator.setDisplayNumber(Double.parseDouble(displayNumberAsTextCache.toString()));
        
        updateViewFromModel();
    }
    
    public void handleClearButtonClick() {
        calculator.resetCalculator();
        resetDisplayTextCache();
        resetLogTextCache();
        updateViewFromModel();
    }
    
    public void handleHistoryButtonClick() {
        calculatorFrame.getTaCalculationHistory().setText("");
        logger.readLog();
        
        String s = logger.getLinesFromLogFile().stream().collect(Collectors.joining("\n"));
        //System.out.println(s);
        calculatorFrame.getTaCalculationHistory().setText(s);
        
//        StringBuffer logFileContent = new StringBuffer("");   
//        int startIndex = 0;
//        
//        if (logger.getLinesFromLogFile().size() > 100) {
//            startIndex = logger.getLinesFromLogFile().size() - 100;
//        }
//        
//        if (logger.getLinesFromLogFile().size() > 100) {
//            startIndex = logger.getLinesFromLogFile().size() - 100;
//        }
//        for (int i = 0; i < logger.getLinesFromLogFile().size(); i++) {
//            logFileContent.append(logger.getLinesFromLogFile().get(i)).append("\n");
//        }
//        calculatorFrame.getTaCalculationHistory().setText(logFileContent.toString());
        

    }

    public void handleSubtractionButtonClick() {
        formattedAppendLogTextCache(OperationSigns.SUBTRACTION);
        calculator.setLastOperation(OperationSigns.SUBTRACTION);
        
        //operation(MathematicOperatoinsUtil.subtract(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
        
        calculator.setDisplayNumber(Double.parseDouble(displayNumberAsTextCache.toString()));
        
        calculator.setNumberInMemory(
            MathematicOperatoinsUtil.subtract(calculator.getDisplayNumber(), calculator.getNumberInMemory())
        );
        calculator.setDisplayNumber(calculator.getNumberInMemory());
        resetDisplayTextCache();
        updateViewFromModel();
    }
    public void handleDivisionButtonClick() {
        formattedAppendLogTextCache(OperationSigns.MULTIPLCATION);
        calculator.setLastOperation(OperationSigns.MULTIPLCATION);
        
        operation(MathematicOperatoinsUtil.multiply(calculator.getNumberInMemory(), calculator.getDisplayNumber()));
       
        resetDisplayTextCache();
        
        updateViewFromModel();
    }

    public void handleSummationButtonClick() {       
        
        formattedAppendLogTextCache(OperationSigns.SUMMATION);
        calculator.setLastOperation(OperationSigns.SUMMATION);
        
        operation(MathematicOperatoinsUtil.sum(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
       
        resetDisplayTextCache();
        
        updateViewFromModel();
    }
    
    
    
    public void handleMultiplicationButtonClick() {
        formattedAppendLogTextCache(OperationSigns.MULTIPLCATION);
        calculator.setLastOperation(OperationSigns.MULTIPLCATION);
        
        operation(MathematicOperatoinsUtil.multiply(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
       
        resetDisplayTextCache();
        
        updateViewFromModel();
    }
    
    public void handleResultButtonClick() {        
        
        if (calculator.getLastOperation() == OperationSigns.SUMMATION) {
            operation(MathematicOperatoinsUtil.sum(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
        }
        
        if (calculator.getLastOperation() == OperationSigns.SUBTRACTION) {
            operation(MathematicOperatoinsUtil.subtract(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
        }
        
        if (calculator.getLastOperation() == OperationSigns.MULTIPLCATION) {
            operation(MathematicOperatoinsUtil.multiply(calculator.getDisplayNumber(), calculator.getNumberInMemory()));
        }
        
        if (calculator.getLastOperation() != OperationSigns.RESULT) {
            formattedAppendLogTextCache(OperationSigns.RESULT);
            logTextCache.append(calculator.getNumberInMemory());
            
            calculator.setLastOperation(OperationSigns.RESULT);
        
            logger.writeLog(logTextCache.toString());
            
            updateViewFromModel();
            resetAll();
        }
    } 
}
