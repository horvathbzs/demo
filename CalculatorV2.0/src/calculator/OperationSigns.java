

package calculator;

import java.text.DecimalFormatSymbols;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public enum OperationSigns {

    SUMMATION("+"), SUBTRACTION("-"), MULTIPLCATION("*"), DIVISION("/"),
    DECIMALSEPARATOR(String.valueOf(new DecimalFormatSymbols().getDecimalSeparator())),
    CLEAR("C"), RESULT("=");
    
    private String sign;
    
    private OperationSigns(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }
    
    
}
