

package calculator;

import java.util.Objects;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class Calculator {

    public final static Double DEFAULT_NUMBER = 0D;
    
    private Double displayNumber;
    private Double numberInMemory;
    private Double result;
    private OperationSigns lastOperation;

    public Calculator() {
        this.displayNumber = DEFAULT_NUMBER;
        this.numberInMemory = DEFAULT_NUMBER;
        this.result = DEFAULT_NUMBER;
    }

    public Double getDisplayNumber() {
        return displayNumber;
    }

    public void setDisplayNumber(Double displayNumber) {
        this.displayNumber = displayNumber;
    }

    public Double getNumberInMemory() {
        return numberInMemory;
    }

    public void setNumberInMemory(Double numberInMemory) {
        this.numberInMemory = numberInMemory;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public OperationSigns getLastOperation() {
        return lastOperation;
    }

    public void setLastOperation(OperationSigns lastOperation) {
        this.lastOperation = lastOperation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.displayNumber);
        hash = 17 * hash + Objects.hashCode(this.numberInMemory);
        hash = 17 * hash + Objects.hashCode(this.result);
        hash = 17 * hash + Objects.hashCode(this.lastOperation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Calculator other = (Calculator) obj;
        if (!Objects.equals(this.displayNumber, other.displayNumber)) {
            return false;
        }
        if (!Objects.equals(this.numberInMemory, other.numberInMemory)) {
            return false;
        }
        if (!Objects.equals(this.result, other.result)) {
            return false;
        }
        if (this.lastOperation != other.lastOperation) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Calculator{" + "displayNumber=" + displayNumber + 
                ", numberInMemory=" + numberInMemory + 
                ", result=" + result + 
                ", lastOperation=" + lastOperation + '}';
    }
    
    public void resetCalculator() {
        this.displayNumber = DEFAULT_NUMBER;
        this.numberInMemory = DEFAULT_NUMBER;
        this.result = DEFAULT_NUMBER;
    }
}
