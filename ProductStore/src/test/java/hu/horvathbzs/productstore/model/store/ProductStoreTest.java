package hu.horvathbzs.productstore.model.store;

import hu.horvathbzs.productstore.model.product.Product;
import hu.horvathbzs.productstore.model.store.ProductStore;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author horvathbzs
 */
@ExtendWith(MockitoExtension.class)
public class ProductStoreTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Product product;

    private ProductStore underTest;

    @BeforeEach
    public void setupTicketStore() {
        underTest = new ProductStore();
    }

    @Test
    public void testAdd() {
        assertAll("ListSize",
                () -> {
                    //When
                    underTest.add(product);

                    //Then
                    assertEquals(1, underTest.getProducts().size());
                },
                () -> {
                    //When
                    underTest.add(product);
                    underTest.add(product);

                    //Then
                    assertEquals(3, underTest.getProducts().size());
                }
        );

    }
    
    

}
