package hu.horvathbzs.productstore.view;

import hu.horvathbzs.productstore.controller.ProductStoreController;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author horvathbzs
 */
public class ProductStoreSwingView extends JFrame implements View {
    
    private ProductStoreController productStoreController;

    @Override
    public void update(String s) {
        
    }

    @Override
    public void setController(ProductStoreController productStoreController) {
        
    }

    @Override
    public void start() {
        
        JPanel jPanel = new JPanel();

        
        add(jPanel);
        
        setSize(400, 50);
        setVisible(true);
    }
    
    
}
