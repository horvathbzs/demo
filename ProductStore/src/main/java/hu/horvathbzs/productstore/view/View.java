package hu.horvathbzs.productstore.view;

import hu.horvathbzs.productstore.controller.ProductStoreController;

/**
 *
 * @author horvathbzs
 */
public interface View {
    
    void update(String s);
    void setController(ProductStoreController productStoreController);
    void start();
    
}
