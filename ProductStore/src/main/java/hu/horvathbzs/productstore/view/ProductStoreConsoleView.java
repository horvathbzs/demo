package hu.horvathbzs.productstore.view;

import hu.horvathbzs.productstore.controller.CommandType;
import hu.horvathbzs.productstore.controller.ProductStoreController;

/**
 *
 * @author horvathbzs
 */
public class ProductStoreConsoleView implements View {

    private ProductStoreController productStoreController;
    
    @Override
    public void update(String s) {
        
    }

    @Override
    public void setController(ProductStoreController productStoreController) {
        this.productStoreController = productStoreController;
    }

    @Override
    public void start() {
        System.out.println(CommandType.ADD + " [specialType (e.g. KITCHEN_DEVICE)]"
                + " [productType (e.g. CHEAP, POPULAR...] [manufacturer] [price (integer)]");
        System.out.println(CommandType.REMOVE + " [productId]");
        System.out.println(CommandType.SAVE);
        System.out.println(CommandType.LOAD);
        System.out.println(CommandType.EXIT + " - quit application");
        System.out.println();
        
        productStoreController.handleConsoleCommands();
    }
    
}
