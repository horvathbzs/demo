package hu.horvathbzs.productstore.controller;


import hu.horvathbzs.productstore.model.product.factory.ProductFactory;
import hu.horvathbzs.productstore.model.store.ProductStore;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */
public class CommandParser {

    private final String SEPARATOR = " ";

    private ProductStore productStore;

    public CommandParser() {
    }

    public CommandParser(ProductStore productStore) {
        this.productStore = productStore;
    }
    
    public void parse(String commandLine) {
        String[] commands = commandLine.split(SEPARATOR);

        if (CommandType.ADD.toString().equals(commands[0])) {
            add(commands, commandLine);
        } else if (CommandType.REMOVE.toString().equals(commands[0])) {

            if (!productStore.getProducts().isEmpty()) {
                productStore.removeById(commands[1]);
            } else {
                System.out.println("Product store is empty, no items removed.");
            }

        } else if (CommandType.LOAD.toString().equals(commands[0])) {
            productStore.load();
        } else if (CommandType.SAVE.toString().equals(commands[0])) {
            productStore.save();
        }
    }

    public void add(String[] commnands, String commandLine) {
        String[] commands = commandLine.split(SEPARATOR);
        if (commands == null || commands.length != 5) {
            throw new IllegalArgumentException("Empty input line");
        } else {
            productStore.add(
                    ProductFactory.createProduct(commands[1], commands[2], commands[3], commands[4])
            );
        }
    }
}
