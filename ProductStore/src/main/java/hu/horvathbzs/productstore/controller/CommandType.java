package hu.horvathbzs.productstore.controller;

/**
 *
 * @author horvathbzs
 */
public enum CommandType {
    ADD, REMOVE, SAVE, LOAD, EXIT
}
