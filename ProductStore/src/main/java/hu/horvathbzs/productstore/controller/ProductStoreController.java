package hu.horvathbzs.productstore.controller;

import hu.horvathbzs.productstore.model.store.ProductStore;
import hu.horvathbzs.productstore.view.View;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author horvathbzs
 */
public class ProductStoreController {
    private View view;
    private ProductStore productStore;

    public ProductStoreController(View view, ProductStore productStore) {
        this.view = view;
        this.productStore = productStore;
        
        view.setController(this);
        productStore.setProductStoreController(this);
        
        view.start();
    }
    
    public void handleConsoleCommands() {

        CommandParser parser = new CommandParser(productStore);

        try (BufferedReader br
                = new BufferedReader(new InputStreamReader(System.in))) {

            String line;

            while ((line = br.readLine()) != null
                    && !CommandType.EXIT.toString().equalsIgnoreCase(line)) {
                parser.parse(line);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
