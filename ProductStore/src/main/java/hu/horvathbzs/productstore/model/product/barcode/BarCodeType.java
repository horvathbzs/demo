

package hu.horvathbzs.productstore.model.product.barcode;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */

public enum BarCodeType {

    DIGITAL, ANALOUGE;
}
