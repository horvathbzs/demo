

package hu.horvathbzs.productstore.model.product;

import hu.horvathbzs.productstore.model.product.type.ProductType;
import hu.horvathbzs.productstore.model.product.ability.Liftable;
import hu.horvathbzs.productstore.model.product.ability.Switchable;
import hu.horvathbzs.productstore.model.product.barcode.BarCodeType;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class CosmeticsDevice extends Product {

    private Double massKg;

    public CosmeticsDevice(String productSpecialGroupType, String productType, String manufacturer, Integer price) {
        super(productSpecialGroupType, productType, manufacturer, price);
        this.massKg = Math.random();
    }
    
    @Override
    public String toString() {
        return "CosmeticsDevice{" + "productType=" + getProductType() + ", manufacturer=" + getManufacturer() + ", barCodeType=" + getBarCodeType() + ", price=" + getPrice() + ", productId=" + getProductId() + ", dateOfPurchase=" + getDateOfPurchase() + "massKg=" + massKg +'}';
    }
}
