package hu.horvathbzs.productstore.model.product;

import hu.horvathbzs.productstore.model.product.type.ProductType;
import hu.horvathbzs.productstore.model.product.ability.Liftable;
import hu.horvathbzs.productstore.model.product.ability.Switchable;
import hu.horvathbzs.productstore.model.product.barcode.BarCodeType;
import hu.horvathbzs.productstore.model.product.type.ProductSpecialGroupType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */
public abstract class Product implements Comparable<Product>, Serializable {

    private static Integer counter = 1;

    private ProductSpecialGroupType productSpecialGroupType;
    private ProductType productType;
    private String manufacturer;
    private BarCodeType barCodeType;
    private Integer price;
    private final String productId;
    private final LocalDate dateOfPurchase;

    public Product(String productSpecialGroupType, String productType, String manufacturer, Integer price) {

        this.productSpecialGroupType = ProductSpecialGroupType.valueOf(productSpecialGroupType);
        this.productType = ProductType.valueOf(productType);
        this.manufacturer = manufacturer;
        setBarCodeType();
        this.price = price;
        this.productId = counter.toString();
        this.dateOfPurchase = LocalDate.now();
        counter++;
    }

    public ProductSpecialGroupType getProductSpecialGroupType() {
        return productSpecialGroupType;
    }

    public void setProductSpecialGroupType(ProductSpecialGroupType productSpecialGroupType) {
        this.productSpecialGroupType = productSpecialGroupType;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BarCodeType getBarCodeType() {
        return barCodeType;
    }

    public void setBarCodeType(BarCodeType barCodeType) {
        this.barCodeType = barCodeType;
    }

    public void setBarCodeType() {
        if (counter % 2 == 0) {
            this.barCodeType = BarCodeType.ANALOUGE;
        } else {
            this.barCodeType = BarCodeType.DIGITAL;
        }
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public LocalDate getDateOfPurchase() {
        return dateOfPurchase;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.productId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.manufacturer, other.manufacturer)) {
            return false;
        }
        if (!Objects.equals(this.productId, other.productId)) {
            return false;
        }
        if (this.productSpecialGroupType != other.productSpecialGroupType) {
            return false;
        }
        if (this.productType != other.productType) {
            return false;
        }
        if (this.barCodeType != other.barCodeType) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        if (!Objects.equals(this.dateOfPurchase, other.dateOfPurchase)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{"
                + "productSpecialGroupType=" + productSpecialGroupType
                + ", productType=" + productType
                + ", manufacturer=" + manufacturer
                + ", barCodeType=" + barCodeType
                + ", price=" + price
                + ", productId=" + productId
                + ", dateOfPurchase=" + dateOfPurchase + '}';
    }

    @Override
    public int compareTo(Product p) {
        if (this == p || this.productId.charAt(0) == p.productId.charAt(0)) {
            return 0;
        } else if (this.productId.charAt(0) < p.productId.charAt(0)) {
            return -1;
        } else {
            return 1;
        }
    }
}
