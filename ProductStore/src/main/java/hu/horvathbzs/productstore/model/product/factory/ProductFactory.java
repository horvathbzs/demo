package hu.horvathbzs.productstore.model.product.factory;

import hu.horvathbzs.productstore.model.product.CosmeticsDevice;
import hu.horvathbzs.productstore.model.product.EntertainmentDevice;
import hu.horvathbzs.productstore.model.product.KitchenDevice;
import hu.horvathbzs.productstore.model.product.Product;
import hu.horvathbzs.productstore.model.product.type.ProductSpecialGroupType;

/**
 *
 * @author horvathbzs
 */
public class ProductFactory {

    public static Product createProduct(String productSpecialGroupType,
            String productType, String manufacturer, String price) {
        
        if (ProductSpecialGroupType.COSMETICS.toString().equals(productSpecialGroupType)) {
            return new CosmeticsDevice(productSpecialGroupType, productType, manufacturer, Integer.parseInt(price));
            
        } else if (ProductSpecialGroupType.ENTERTAINMENT_DEVICE.toString().equals(productSpecialGroupType)) {
            return new EntertainmentDevice(productSpecialGroupType, productType, manufacturer, Integer.parseInt(price));
            
        } else if (ProductSpecialGroupType.KITCHEN_DEVICE.toString().equals(productSpecialGroupType)) {
            return new KitchenDevice(productSpecialGroupType, productType, manufacturer, Integer.parseInt(price));
        }
        
        return null;
    }
}
