

package hu.horvathbzs.productstore.model.product.exception;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class ProductNotAbleToSwitchOnException extends Exception {

    public ProductNotAbleToSwitchOnException(String message) {
        super(message);
    }
}
