package hu.horvathbzs.productstore.model.store;

import hu.horvathbzs.productstore.controller.ProductStoreController;
import hu.horvathbzs.productstore.model.product.Product;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */
public class ProductStore<T extends Product> implements Serializable {

    private ProductStoreController productStoreController;
    File file = new File("product_store.ser");

    private List<T> products = new ArrayList();

    public List<T> getProducts() {
        return products;
    }

    public void setProductStoreController(ProductStoreController productStoreController) {
        this.productStoreController = productStoreController;
    }

    public void add(T product) {
        products.add(product);
    }

    public void remove(T product) {
        products.remove(product);
    }

    public void removeById2(String id) {
        Object[] optProduct = products.stream()
                .filter(p -> p.getProductId().equals(id))
                .toArray();

        T product = (T) optProduct[0];
        remove(product); 
    }
    
    public void removeById (String id) {
        Optional<T> product = products.stream()
                .findFirst()
                .filter(p -> p.getProductId().equals(id));
        
        remove(product.get());
    }


    public void save() {
        FileOutputStream fileout;
        ObjectOutputStream objectout;
        try {
            fileout = new FileOutputStream(file);
            objectout = new ObjectOutputStream(fileout);

            objectout.writeObject(products);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void load() {
        FileInputStream filein;
        ObjectInputStream objectin;
        try {
            filein = new FileInputStream(file);
            objectin = new ObjectInputStream(filein);
            List<Product> products = (List<Product>) objectin.readObject();

            System.out.println(products);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        return products.toString();
    }
}
