package hu.horvathbzs.productstore.model.product.type;

/**
 *
 * @author horvathbzs
 */
public enum ProductSpecialGroupType {
    KITCHEN_DEVICE, ENTERTAINMENT_DEVICE, COSMETICS
}
