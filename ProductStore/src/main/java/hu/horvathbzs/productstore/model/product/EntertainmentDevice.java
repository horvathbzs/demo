

package hu.horvathbzs.productstore.model.product;

import hu.horvathbzs.productstore.model.product.type.ProductType;
import hu.horvathbzs.productstore.model.product.ability.Liftable;
import hu.horvathbzs.productstore.model.product.ability.Switchable;
import hu.horvathbzs.productstore.model.product.barcode.BarCodeType;
import hu.horvathbzs.productstore.model.product.exception.ProductNotAbleToSwitchOnException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class EntertainmentDevice extends Product implements Liftable, Switchable {

    private static Integer switchOnCounter = 0;
    
    public EntertainmentDevice(String productSpecialGroupType, String productType, String manufacturer, Integer price) {
        super(productSpecialGroupType, productType, manufacturer, price);
    }

    @Override
    public String toString() {
        return "EntertainmentDevice{" + "productType=" + getProductType() + ", manufacturer=" + getManufacturer() + ", barCodeType=" + getBarCodeType() + ", price=" + getPrice() + ", productId=" + getProductId() + ", dateOfPurchase=" + getDateOfPurchase() + '}';
    }  

    @Override
    public void lift() {
        System.out.println("I am a liftable EntertainmentDevice.");
    }

    @Override
    public void switchOn() throws ProductNotAbleToSwitchOnException {
        ++switchOnCounter;
        
        if (switchOnCounter >= 5) {
                throw new ProductNotAbleToSwitchOnException("Device went wrong");
        } else {
            System.out.println("I am a switchable EntertainmentDevice.");
        }      
    }

    
    
}
