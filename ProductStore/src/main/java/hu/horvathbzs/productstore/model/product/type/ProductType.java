

package hu.horvathbzs.productstore.model.product.type;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public enum ProductType {

    CHEAP, AVERAGE, EXPENSIVE, LUXURIOUS, POPULAR;
}
