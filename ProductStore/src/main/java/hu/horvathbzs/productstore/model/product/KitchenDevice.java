

package hu.horvathbzs.productstore.model.product;

import hu.horvathbzs.productstore.model.product.type.ProductType;
import hu.horvathbzs.productstore.model.product.ability.Liftable;
import hu.horvathbzs.productstore.model.product.ability.Switchable;
import hu.horvathbzs.productstore.model.product.barcode.BarCodeType;

/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class KitchenDevice extends Product implements Liftable, Switchable {

    public KitchenDevice(String productSpecialGroupType, String productType, String manufacturer, Integer price) {
        super(productSpecialGroupType, productType, manufacturer, price);
    }

    @Override
    public String toString() {
        return "KitchenDevice{" + "productType=" + getProductType() + ", manufacturer=" + getManufacturer() + ", barCodeType=" + getBarCodeType() + ", price=" + getPrice() + ", productId=" + getProductId() + ", dateOfPurchase=" + getDateOfPurchase() + '}';
    }
    
    

    @Override
    public void lift() {
        System.out.println("I am a liftable KitchenDevice.");
    }

    @Override
    public void switchOn() {
        System.out.println("I am a switchable KitchenDevice.");
    }    
}
