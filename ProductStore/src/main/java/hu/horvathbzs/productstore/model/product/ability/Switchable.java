/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.horvathbzs.productstore.model.product.ability;

import hu.horvathbzs.productstore.model.product.exception.ProductNotAbleToSwitchOnException;

/**
 *
 * @author B
 */
public interface Switchable {
    void switchOn() throws ProductNotAbleToSwitchOnException;
}
