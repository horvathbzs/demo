package hu.horvathbzs.productstore;

import hu.horvathbzs.productstore.controller.ProductStoreController;
import hu.horvathbzs.productstore.model.store.ProductStore;
import hu.horvathbzs.productstore.view.ProductStoreConsoleView;
import hu.horvathbzs.productstore.view.View;




/**
 *
 * @author Horvath Balazs horvathbzs@gmail.com
 */


public class ConsoleApp {

    public static void main(String[] args) {
        
        ProductStore productStore = new ProductStore();
        View consoleView = new ProductStoreConsoleView();
        
        new ProductStoreController(consoleView, productStore);
        
    }
    
}
