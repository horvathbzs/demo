package hitgame.application;

import hitgame.model.Warrior;
import hitgame.service.FightService;
import java.util.Scanner;

/**
 *
 * @author horvathbzs
 */
public class HitGame {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Warrior swordsman = new Warrior("swordsman", 60, 7, 2, 1, 0);
        Warrior archer = new Warrior("archer", 35, 3, 12, 0, 0);

        new FightService(swordsman, archer).start();

    }

}
