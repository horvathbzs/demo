package hitgame.service;

import hitgame.model.Warrior;
import java.util.Scanner;

/**
 *
 * @author horvathbzs
 */
public class FightService {

    private Warrior warrior1;
    private Warrior warrior2;
    private String attackMode;
    Scanner scanner = new Scanner(System.in);

    public FightService(Warrior warrior1, Warrior warrior2) {
        this.warrior1 = warrior1;
        this.warrior2 = warrior2;
    }

    public void start() {

        System.out.println("Melee attack: A");
        System.out.println("Ranged attack: S");
        System.out.println("Exit = Q");

        writeStatus();
        System.out.println("\nPush any button to start");
        attackMode = scanner.next();
        while ( ! "Q".equalsIgnoreCase(attackMode)) {
            fight();
        }

    }

    public void meleeAttack(Warrior attacker, Warrior defender) {
        attacker.meleeAttack(defender);
    }

    public void rangedAttack(Warrior attacker, Warrior defender) {
        attacker.rangedAttack(defender);
    }

    public void fight() {
        doOneHit(this.warrior1, this.warrior2);
        doOneHit(this.warrior2, this.warrior1);
    }
    
    public void doOneHit(Warrior attacker, Warrior defender) {
        if (attacker.getHealthPoint() <= 0 || defender.getHealthPoint() <= 0) {
            System.exit(0);
        }
    
        
        System.out.println();
            System.out.println(attacker.getName() + "'s turn");
            attackMode = scanner.next();
            oneHit(attacker, defender, attackMode);
    }

    public void oneHit(Warrior attacker, Warrior defender, String attackMode) {

        if ("A".equals(attackMode)) {
            meleeAttack(attacker, defender);
        }
        if ("S".equals(attackMode)) {
            rangedAttack(attacker, defender);
        }

        writeStatus();
    }

    public void writeStatus() {
        System.out.println();
        System.out.println(this.warrior1.getName() + "'s health = " + this.warrior1.getHealthPoint());
        System.out.println(this.warrior2.getName() + "'s health = " + this.warrior2.getHealthPoint());
    }
}
