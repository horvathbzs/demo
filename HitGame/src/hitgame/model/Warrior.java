package hitgame.model;

import java.util.Objects;

/**
 *
 * @author horvathbzs
 */
public class Warrior {
    
    private String name;
    private Integer healthPoint;
    private Integer meeleAttackPoint;
    private Integer rangedAttackPoint;
    private Integer armorPoint;
    private Integer pierceArmorPoint;

    public Warrior() {
    }

    public Warrior(String name, Integer health, Integer meeleAttack, Integer rangedAttack, Integer armor, Integer pierceArmor) {
        this.name = name;
        this.healthPoint = health;
        this.meeleAttackPoint = meeleAttack;
        this.rangedAttackPoint = rangedAttack;
        this.armorPoint = armor;
        this.pierceArmorPoint = pierceArmor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(Integer healthPoint) {
        this.healthPoint = healthPoint;
    }

    public Integer getMeeleAttackPoint() {
        return meeleAttackPoint;
    }

    public void setMeeleAttackPoint(Integer meeleAttackPoint) {
        this.meeleAttackPoint = meeleAttackPoint;
    }

    public Integer getRangedAttackPoint() {
        return rangedAttackPoint;
    }

    public void setRangedAttackPoint(Integer rangedAttackPoint) {
        this.rangedAttackPoint = rangedAttackPoint;
    }

    public Integer getArmorPoint() {
        return armorPoint;
    }

    public void setArmorPoint(Integer armorPoint) {
        this.armorPoint = armorPoint;
    }

    public Integer getPierceArmorPoint() {
        return pierceArmorPoint;
    }

    public void setPierceArmorPoint(Integer pierceArmorPoint) {
        this.pierceArmorPoint = pierceArmorPoint;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.healthPoint);
        hash = 47 * hash + Objects.hashCode(this.meeleAttackPoint);
        hash = 47 * hash + Objects.hashCode(this.rangedAttackPoint);
        hash = 47 * hash + Objects.hashCode(this.armorPoint);
        hash = 47 * hash + Objects.hashCode(this.pierceArmorPoint);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Warrior other = (Warrior) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.healthPoint, other.healthPoint)) {
            return false;
        }
        if (!Objects.equals(this.meeleAttackPoint, other.meeleAttackPoint)) {
            return false;
        }
        if (!Objects.equals(this.rangedAttackPoint, other.rangedAttackPoint)) {
            return false;
        }
        if (!Objects.equals(this.armorPoint, other.armorPoint)) {
            return false;
        }
        if (!Objects.equals(this.pierceArmorPoint, other.pierceArmorPoint)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Warrior{" + "name=" + name + ", healthPoint=" + healthPoint + ", meeleAttackPoint=" + meeleAttackPoint + ", rangedAttackPoint=" + rangedAttackPoint + ", armorPoint=" + armorPoint + ", pierceArmorPoint=" + pierceArmorPoint + '}';
    }

    
    
    public void meleeAttack(Warrior enemy) {
        Integer enemyHealthPointDecrease = 0;
        if (this.getMeeleAttackPoint() > enemy.getArmorPoint()) {
            enemyHealthPointDecrease = this.getMeeleAttackPoint() - enemy.getArmorPoint();
        } else {
            enemyHealthPointDecrease = 0;
        }
        
        enemy.setHealthPoint(enemy.getHealthPoint() - enemyHealthPointDecrease);
    }
    
    public void rangedAttack(Warrior enemy) {
        Integer enemyHealthPointDecrease = 0;
        if (this.getRangedAttackPoint() > enemy.getPierceArmorPoint()) {
            enemyHealthPointDecrease = this.getRangedAttackPoint() - enemy.getPierceArmorPoint();
        } else {
            enemyHealthPointDecrease = 0;
        }
        
        enemy.setHealthPoint(enemy.getHealthPoint() - enemyHealthPointDecrease);
    }
}
