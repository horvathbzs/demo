package hu.horvathbzs.hikingreport.controller;

import hu.horvathbzs.hikingreport.dto.HikerFindAllResponse;
import hu.horvathbzs.hikingreport.service.HikerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author horvathbzs
 */

@RestController
@RequestMapping("hikingreport")
public class HikerRestController {
    
    @Autowired
    private HikerService hikerService;
    
    @RequestMapping(method = RequestMethod.GET, path = "hello")
    public String hello() {
        return "Hello";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "all_hikers")
    public ResponseEntity findAllHikers() {
        HikerFindAllResponse allHikers = new HikerFindAllResponse();
        allHikers.setAllHikers(hikerService.findAllHikers());
        
        return ResponseEntity.ok(allHikers);
    }
}
