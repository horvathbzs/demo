package hu.horvathbzs.hikingreport;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author horvathbzs
 */

@Configuration
@EnableJpaRepositories(basePackages = "hu.horvathbzs.hikingreport.repository")
@ComponentScan(basePackages = "hu.horvathbzs.hikingreport")
public class Config {

}
