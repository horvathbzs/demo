package hu.horvathbzs.hikingreport.entity;


import javax.persistence.Column;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "hiker")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hiker {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "familyname")
    private String familyName;
    
    @Column(name = "givenname1")
    private String givenName1;
    
    @Column(name = "givenname2")
    private String givenName2;

    public Hiker(Long id) {
        this.id = id;
    }
}
