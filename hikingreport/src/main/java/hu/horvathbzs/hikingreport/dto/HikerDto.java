package hu.horvathbzs.hikingreport.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HikerDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("familyName")
    private String familyName;
    
    @JsonProperty("givenName1")
    private String givenName1;
    
    @JsonProperty("givenName2")
    private String givenName2;
}
