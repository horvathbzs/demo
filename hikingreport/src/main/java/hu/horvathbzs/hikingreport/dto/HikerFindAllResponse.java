package hu.horvathbzs.hikingreport.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author horvathbzs
 */

@Data
public class HikerFindAllResponse {
    
    @JsonProperty("allHikers")
    private List<HikerDto> allHikers = new ArrayList<>();
}
