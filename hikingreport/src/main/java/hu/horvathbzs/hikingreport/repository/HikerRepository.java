package hu.horvathbzs.hikingreport.repository;

import hu.horvathbzs.hikingreport.entity.Hiker;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author horvathbzs
 */

@Repository
public interface HikerRepository extends JpaRepository<Hiker, Long>{
   
    public List<Hiker> findAll();
    
}
