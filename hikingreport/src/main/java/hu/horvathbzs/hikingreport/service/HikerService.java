package hu.horvathbzs.hikingreport.service;

import hu.horvathbzs.hikingreport.dto.HikerDto;
import hu.horvathbzs.hikingreport.entity.Hiker;
import hu.horvathbzs.hikingreport.repository.HikerRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author horvathbzs
 */
@Service
public class HikerService {

    private HikerRepository hikerRepository;

    @Autowired
    public HikerService(HikerRepository hikerRepository) {
        this.hikerRepository = hikerRepository;
    }

    public List<HikerDto> findAllHikers() {
        List<HikerDto> hikerDtos = new ArrayList<>();
        List<Hiker> hikers = hikerRepository.findAll();

        hikers.forEach(
                entity -> {
                    HikerDto dto = new HikerDto();
                    BeanUtils.copyProperties(entity, dto);
                    hikerDtos.add(dto);
                });

        return hikerDtos;
    }

}
